use std::env::args;
use std::net::SocketAddr;
use warp::Filter;

#[tokio::main]
async fn main() {
    let addr: SocketAddr = args()
        .nth(1)
        .unwrap_or_else(|| "localhost:8080".into())
        .parse()
        .unwrap();

    let hello = warp::any().map(|| format!("hello world"));

    warp::serve(hello).run(addr).await;
}
