import time
import process_util
import json
import measurement

def measure(pid):
  """
  Uses `ps --libxo=json` on FreeBSD to acquire performance related information on a process.
  """
  try:
    taken_at = time.time()
    cmd = 'ps -x --libxo=json -o pid,cpu,pcpu,cputime,etime,etimes,systime,usertime,time,vsz,rss -p %s' % (pid)
    output = process_util.execute_cmd(cmd)
    data = json.loads(output)

    if 'process-information' in data:
      entry = next(filter(lambda e: int(e['pid']) == pid.as_int(), data['process-information']['process']))
      return measurement.Measurement(
        cpu_time = __parse_cpu_time(entry['cpu-time']),
        user_time = __parse_cpu_time(entry['user-time']),
        system_time = __parse_cpu_time(entry['system-time']),
        elapsed_time = __parse_cpu_time(entry['elapsed-time']),
        cpu_usage = float(entry['percent-cpu']),
        vss_mib = float(entry['virtual-size']) / 1024.0,
        rss_mib = float(entry['rss']) / 1024.0,
        num_processes = 1,
        taken_at = [taken_at])
    else:
      return None
  except (KeyError, TypeError) as err:
    print("Exception!", err)
    return None

def __parse_cpu_time(string):
  if string == '-':
    return 0.0
  arr = string.split(":")
  arr.reverse()
  seconds = float(arr[0]) if arr[0:] else 0.0
  minutes = int(arr[1]) if arr[1:] else 0
  hours = int(arr[2]) if arr[2:] else 0
  return seconds + (60 * minutes) + (3600 * hours)
