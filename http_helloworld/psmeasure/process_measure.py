from functools import reduce
import measurement
import sys

if sys.platform.startswith("freebsd12"):
    import ps_list_freebsd as ps_list
    import ps_measure_freebsd as ps_measure
else:
    import ps_list_psutil as ps_list
    import ps_measure_psutil as ps_measure

class ProcessMeasure:
  def __init__(self, list_mod = ps_list, measure_mod = ps_measure):
    self.list_fn = list_mod.list_recursive_child_processes_of
    self.measure_fn = measure_mod.measure

  def measure_recursive(self, pid):
    all_pids = self.list_fn(pid)
    if len(all_pids) > 0:
      return reduce(measurement.accumulate, map(lambda pid: self.measure_fn(pid), all_pids), None)
    else:
      return None
