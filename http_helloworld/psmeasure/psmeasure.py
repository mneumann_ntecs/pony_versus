import process_measure as ps
import process_recorder as pr
import process_util
import time
import sys
import json
import subprocess

class Recorder:
    def record_entry(self, measurement):
        print(json.dumps(measurement.as_dict()))

cmd = sys.argv[1]
duration = int(sys.argv[2])
interval = float(sys.argv[3])

with subprocess.Popen(cmd.split(), cwd = None, stdin = subprocess.DEVNULL, stdout = subprocess.PIPE, stderr = subprocess.PIPE) as proc:
  recorder = pr.ProcessRecorder(ps.ProcessMeasure(), process_util.Pid(proc.pid), Recorder(), interval = interval) 
  recorder.start()

  try:
    outs, errs = proc.communicate(timeout=duration)
  except subprocess.TimeoutExpired:
    proc.kill()
    outs, errs = proc.communicate()

  recorder.stop()
