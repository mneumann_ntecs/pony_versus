import time
import measurement
import psutil

def measure(pid):
  """
  Uses Python's `psutil` library to acquire performance related information on a process.
  """
  try:
    p = psutil.Process(pid.pid)

    taken_at = time.time()
    cpu_times = p.cpu_times()
    mem = p.memory_info()

    return measurement.Measurement(
      cpu_time = cpu_times.user + cpu_times.system,
      user_time = cpu_times.user,
      system_time = cpu_times.system,
      elapsed_time = taken_at - p.create_time(),
      cpu_usage = p.cpu_percent(),
      vss_mib = mem.vms / (1024.0 * 1024.0),
      rss_mib = mem.rss / (1024.0 * 1024.0),
      num_processes = 1,
      taken_at = [taken_at])
  except (psutil.NoSuchProcess):
    return None
