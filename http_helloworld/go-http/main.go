package main

import (
    "fmt"
    "net/http"
    "os"
)

func main() {
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "hello world")
    })

    var host string = "localhost:8080"

    if len(os.Args[1:]) >= 1 {
      host = os.Args[1]
    }

    http.ListenAndServe(host, nil)
}
