#!/bin/sh

LOCALHOST=192.168.1.7
REMOTEHOST=192.168.1.8

STARTPORT=9500
WRK_DURATION=60
BENCH_DURATION=65
PAUSE=10

command_impl() {
  local _impl=$1
  local _port=$2

  if [ "${_impl}" = "go" ]; then
    echo "./go-http/go-http ${LOCALHOST}:${_port}"
  elif [ "${_impl}" = "rust" ]; then
    echo "./rust-http/target/release/rust-http ${LOCALHOST}:${_port}"
  elif [ "${_impl}" = "pony" ]; then
    echo "./pony-http/pony-http  ${LOCALHOST} ${_port}"
  else
    exit 1
  fi
}

mkdir -p ./results
port=${STARTPORT}

for concurrency in 100 500 1000; do
  for reqs in 10000 50000 100000 200000; do
    for impl in pony go rust; do
      cmd=`command_impl ${impl} ${port}` 

      echo "impl: ${impl}"
      echo "reqs/s: ${reqs}"
      echo "-c: ${concurrency}"
      echo "cmd: ${cmd}" 
      echo "port: ${port}"

      python3.7 psmeasure/psmeasure.py "${cmd}" ${BENCH_DURATION} 1 > results/${impl}_${reqs}_${concurrency}.json &
      bench=$!
      ssh ${REMOTEHOST} "wrk2 --latency -c${concurrency} -t8 -d${WRK_DURATION}s -R${reqs} http://${LOCALHOST}:${port}" > \
        results/${impl}_${reqs}_${concurrency}.txt
      wait $bench

      echo Sleeping...
      sleep ${PAUSE}

      port=$(expr ${port} + 1)
    done
  done
done
